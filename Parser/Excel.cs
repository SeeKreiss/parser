﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace Parser
{
    class Excel
    {
        string path = "";
        _Application excel = new _Excel.Application();
        Workbook wb;
        Worksheet ws;

        public Excel(string path)
        {
            this.path = path;
            wb = excel.Workbooks.Open(path);
            ws = wb.Worksheets[1];
        }
        public string ReadCell(int i, int j)
        {
            if (ws.Cells[i, j].Value != null)
                return (ws.Cells[i, j].Value).ToString();
            else
                return "";
        }
        public void Close()
        {
            wb.Close(0);
        }
        public static void DownFile()
        {
            try
            {


                using (WebClient webClient = new WebClient())
                {
                    if (File.Exists(@"C:\Education\thrlist.xlsx"))
                    File.Delete(@"C:\Education\thrlist.xlsx");
                    Directory.CreateDirectory(@"C:\Education\");
                    webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", @"C:\Education\thrlist.xlsx");
                }
                MessageBox.Show("Файл загружен");
            }
            catch (Exception)
            {
                MessageBox.Show("Нет подключения к интернету");
            }
        }
    }
}
