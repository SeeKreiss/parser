﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser
{
    public partial class ListOfDanger : Form
    {
        static int pageSize = 15;
        static int i = 1;
        static int pageCount = Danger.pad.Count() / pageSize + 1;
        static int page = 1;
        public ListOfDanger()
        {
            InitializeComponent();
            try
            {

                label1.Text = page.ToString();
                for (i = 1; i <= pageSize; i++)
                {
                    dataGridView1.Rows.Add(Danger.pad[i].ID, Danger.pad[i].Name);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //предыдущая страница
            try
            {
                if (page != 1)
                {
                    page--;
                    label1.Text = page.ToString();
                    dataGridView1.Rows.Clear();
                    for (i -= 30; i <= pageSize*page; i++)
                    {
                        dataGridView1.Rows.Add(Danger.pad[i].ID, Danger.pad[i].Name);
                    }
                }
            }
            catch (Exception s)
            {
                MessageBox.Show(s.Message);
            }




        }

        private void button2_Click(object sender, EventArgs e)
        {
            //следующая страница
            try
            {


                if (page != pageCount)
                {
                    page++;
                    label1.Text = page.ToString();
                    dataGridView1.Rows.Clear();
                    while ((i <= pageSize * page)&&(i<= Danger.pad.Count()))
                    {
                        dataGridView1.Rows.Add(Danger.pad[i].ID, Danger.pad[i].Name);
                        i++;
                    }
                }
            }
            catch (Exception d)
            {
                MessageBox.Show(d.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
