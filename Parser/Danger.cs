﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Parser
{
    public class Danger
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string Source { set; get; }
        public string Obj { set; get; }
        public string Confident { set; get; }
        public string Intgrity { set; get; }
        public string Acсess { set; get; }

        /*public Danger(int iD, string name, string description, string source, string obj, bool confident, bool intgrity, bool acсess)
        {
            ID = iD;
            Name = name;
            Description = description;
            Source = source;
            Obj = obj;
            Confident = confident;
            Intgrity = intgrity;
            Acсess = acсess;
        } */
        public string YesNo(string a)
        {
            if (a == "1")
                return "Да";
            else
                return "Нет";
        }
        public static List<string> lines = new List<string>();
        public static Dictionary<int, Danger> pad = new Dictionary<int, Danger>();
        static Dictionary<int, Danger> olDpad = new Dictionary<int, Danger>();
        public static string pathxls = @"C:\Education\thrlist.xlsx";
        public static string path = @"C:\Education\Parsed.txt";

        public static void CheckFile()
        {
            string curFile = path;
            if (File.Exists(curFile))
            {

            }
            else
            {
                StartDialog frm = new StartDialog();
                frm.ShowDialog();
                //метод парса
                try
                {

                    Parsing();
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.Message);
                }
            }
        }

        public static void Parsing()
        {
            XlsToDict(pad);
            SerDict(pad);
        }
        public static void XlsToDict(Dictionary<int, Danger> pad)
        {
            MessageBox.Show("Начинается считывание, подождите немного");
            pad.Clear();
            Excel excel = new Excel(pathxls);
            int i = 3;
            int j = 1;
            while (excel.ReadCell(i, 1) != "") //заполнение словаря из Excel
            {
                Danger danger = new Danger();
                danger.ID = Int32.Parse(excel.ReadCell(i, j++));
                danger.Name = excel.ReadCell(i, j++);
                danger.Description = excel.ReadCell(i, j++);
                danger.Source = excel.ReadCell(i, j++);
                danger.Obj = excel.ReadCell(i, j++);
                danger.Confident = danger.YesNo(excel.ReadCell(i, j++));
                danger.Intgrity = danger.YesNo(excel.ReadCell(i, j++));
                danger.Acсess = danger.YesNo(excel.ReadCell(i, j));
                j = 1;
                pad.Add(danger.ID, danger);
                i++;
            }
            excel.Close();
        }
        public static void SerDict(Dictionary<int, Danger> pad) //запись из словаря в текстовый файл
        {
            using (FileStream fs = File.Create(path))
            { }
            using (StreamWriter sw = File.AppendText(path)) //запись в файл
            {
                foreach (var a in pad)
                {
                    string JSONresult = JsonConvert.SerializeObject(a.Value);
                    sw.WriteLine(JSONresult);
                }
            }
            MessageBox.Show("Файл создан и заполнен");
        }

        public static void DeserFromFile(Dictionary<int, Danger> pad) //read from text file
        {
            try
            {

            
            pad.Clear();



            string[] lines = File.ReadAllLines(path);
            int i = 1;
            foreach (var a in lines)
            {
                pad.Add(i, JsonConvert.DeserializeObject<Parser.Danger>(a));
                i++;
            }
            }
            catch (FileNotFoundException h)
            {
                MessageBox.Show(h.Message);
                MessageBox.Show("Попробуйте сначала загрузить файл");
            }
        }
        public static void Updating()
        {
            int i = 1;
            int j = 0;
            lines.Clear();
            DeserFromFile(olDpad);
            Excel.DownFile();
            if (File.Exists(pathxls))
            {
                XlsToDict(pad);


                foreach (var a in pad)
                {
                    if (olDpad.ContainsKey(i)) //проверка существует ли такая угроза в старом файле
                    {
                        if ((!a.Value.ID.Equals(olDpad[i].ID)))
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменен идентификатор угрозы " + olDpad[i].ID + "-" + a.Value.ID);
                            j++;
                        }
                        if ((!a.Value.Name.Equals(olDpad[i].Name))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено наименование " + olDpad[i].Name + " - " + a.Value.Name);
                            j++;
                        }
                        if ((!a.Value.Description.Equals(olDpad[i].Description))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено описание " + olDpad[i].Description + " - " + a.Value.Description);
                            j++;
                        }
                        if ((!a.Value.Source.Equals(olDpad[i].Source))) //проверка отдельных записей
                        {

                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменен источник угрозы " + olDpad[i].Source + " - " + a.Value.Source);
                            j++;
                        }
                        if ((!a.Value.Obj.Equals(olDpad[i].Obj))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено объект воздействия " + olDpad[i].Obj + " - " + a.Value.Obj);
                            j++;
                        }
                        if ((!a.Value.Confident.Equals(olDpad[i].Confident))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено значение нарушения конфиденциальности " + olDpad[i].Confident + " - " + a.Value.Confident);
                            j++;
                        }
                        if ((!a.Value.Intgrity.Equals(olDpad[i].Intgrity))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено значение нарушения целостности " + olDpad[i].Intgrity + " - " + a.Value.Intgrity);
                            j++;
                        }
                        if ((!a.Value.Acсess.Equals(olDpad[i].Acсess))) //проверка отдельных записей
                        {
                            lines.Add("Изменена угроза с идентификатором: " + a.Value.ID + "Изменено значение нарушения доступности " + olDpad[i].Acсess + " - " + a.Value.Acсess);
                            j++;
                        }

                    }
                    else
                    {
                        j++;
                        lines.Add("Появилась новая угроза с идентификатором: " + a.Value.ID + " и наименованием: " + a.Value.Name);
                        //сообщение о появлении новой угрозы
                    }
                    i++;

                }
                if (j == 0)
                {
                    lines.Add("Изменений не обнаружено");
                }
                else
                {
                    lines.Add("Общее количество изменений: " + j);
                }
                SerDict(pad);
            }
            
            
        }

        public static void Save()
        {
            try
            { 
            DeserFromFile(pad);
                if (pad.Count() != 0)
                {
                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.Filter = "Text file (*.txt)|*.txt";
                    if (dlg.ShowDialog() == true)
                    {
                        using (FileStream fs = File.Create(dlg.FileName))
                        { }
                        using (StreamWriter sw = File.AppendText(dlg.FileName)) //запись в файл
                        {
                            foreach (var a in pad)
                            {
                                sw.WriteLine(a.Value.ToString());
                            }
                        }
                    }
                }           
            }
            catch (Exception g)
            {
                MessageBox.Show(g.Message);
                
            }

        }




        public override string ToString()
        {
            return ID + " " + Name + " " + Description + " " + Source + " " + Obj + " " + Confident + " " + Intgrity + " " + Acсess;
        }
    }
}
