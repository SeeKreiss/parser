﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Parser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Danger.CheckFile();
        }

        
        
        

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            //Update button

            try
            {
                if (File.Exists(Danger.path))
                {
                    UpdateList frm = new UpdateList();
                    frm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Отсутствует локальная база, воспользуйтесь загрузкой файла");
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }


        }

        private void Button_Click_2(object sender, RoutedEventArgs e) //кнопка для тестов
        {
            try
            {
                Close();

            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //кнопка просмотра перечня угроз

            Danger.DeserFromFile(Danger.pad);

            if (Danger.pad.Count() != 0)
            {
                ListOfDanger frm = new ListOfDanger();
                frm.ShowDialog();
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            //кнопка просмотра угрозы
            if (File.Exists(Danger.path))
            {
                FullInfo frm = new FullInfo();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Сначала загрузите файл");
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            //загрузка файла
            Excel.DownFile();
            Danger.Parsing();
        
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            //сохранение на жестком диске
            Danger.Save();
        }
    }
}
