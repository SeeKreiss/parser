﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser
{
    public partial class FullInfo : Form
    {
        public FullInfo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                //ввод индификатора
                int i;
                Danger.DeserFromFile(Danger.pad);
                if (Int32.TryParse(textBox1.Text, out i) && (i>0 && (i< Danger.pad.Count())))
                {
                    dataGridView1.Rows.Clear();
                    dataGridView1.Rows.Add("Идентификатор", Danger.pad[i].ID);
                    dataGridView1.Rows.Add("Наименование", Danger.pad[i].Name);
                    dataGridView1.Rows.Add("Описание", Danger.pad[i].Description);
                    dataGridView1.Rows.Add("Источник угрозы", Danger.pad[i].Source);
                    dataGridView1.Rows.Add("Объект воздействия", Danger.pad[i].Obj);
                    dataGridView1.Rows.Add("Нарушение конфиденциальности", Danger.pad[i].Confident);
                    dataGridView1.Rows.Add("Нарушение целостности", Danger.pad[i].Intgrity);
                    dataGridView1.Rows.Add("Нарушение доступности", Danger.pad[i].Acсess);
                }
                
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }
}
