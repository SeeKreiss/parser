﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser
{
    public partial class UpdateList : Form
    {
        public UpdateList()
        {
            try
            {
                InitializeComponent();
                if (File.Exists(Danger.path))
                {


                    Danger.Updating();

                    MessageBox.Show("Обновление прошло успешно");

                    for (int i = 0; i < Danger.lines.Count(); i++)
                    {

                        dataGridView1.Rows.Add();
                        dataGridView1.Rows[i].Cells[0].Value = Danger.lines[i];
                    }
                }
                else
                {
                    MessageBox.Show("Попробуйте создать локальный файл заново");
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при обновлении");
            }

        }
    }
}
